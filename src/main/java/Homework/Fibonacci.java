package homework;


/**
 * Class for building Fibonacci row.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public class Fibonacci {
    /**
     * @param PERCENT - 100%
     */
    private static final int PERCENT = 100;
    /**
     * @param SIZE - max value of interval
     */
    private static final int SIZE = 500;
    /**
     * @param f1 - the biggest odd fibonacci number in interval
     */
    private static int f1 = 0;
    /**
     * @param f2 - the biggest even fibonacci number in interval
     */
    private static int f2 = 0;
    /**
     * @param fibonacci - row of fibonacci numbers
     */
    private int[] fibonacci = new int[SIZE];
    /**
     * @param number - Interval object
     */
    private static Interval number = new Interval();

    /**
     * @return int Access to f1
     * @see Fibonacci#accessF1()
     */
    final int accessF1() {
        return f1;
    }

    /**
     * @return int Access to f2
     * @see Fibonacci#accessF2()
     */
    final int accessF2() {
        return f2;
    }

    /**
     * @see Fibonacci#fibonacciBuilding()
     */
    final void fibonacciBuilding() {
        int n = -1;
        int fibonPrevious = 0;
        int fibonCurrent = 1;
        while (n < 0) {
            n = Interval.entering();
        }
        fibonacci[0] = fibonPrevious;
        fibonacci[1] = fibonCurrent;
        for (int i = 2; i < n; i++) {
            int copy = fibonCurrent;
            fibonacci[i] += fibonPrevious + fibonCurrent;
            fibonCurrent += fibonPrevious;
            fibonPrevious = copy;
        }
    }

    /**
     * @return int The biggest odd fibonacci number in interval
     * @see Fibonacci#findingTheBiggestOddFibonacci()
     */
    final int findingTheBiggestOddFibonacci() {
        int lastOdd = -1;
        for (int i = 0; i < fibonacci.length; i++) {
            if (fibonacci[i] <= number.lastNumAccess()) {
                if (fibonacci[i] >= number.firstNumAccess()) {
                    if (fibonacci[i] % 2 != 0) {
                        lastOdd = fibonacci[i];
                    }
                }
            } else {
                break;
            }
        }
        f1 = lastOdd;
        return f1;
    }

    /**
     * @return int The biggest even fibonacci number in interval
     * @see Fibonacci#findingTheBiggestEvenFibonacci()
     */
    final int findingTheBiggestEvenFibonacci() {
        int lastEven = -1;
        for (int i = 0; i < fibonacci.length; i++) {
            if (fibonacci[i] <= number.lastNumAccess()) {
                if (fibonacci[i] >= number.firstNumAccess()) {
                    if (fibonacci[i] % 2 == 0) {
                        lastEven = fibonacci[i];
                    }
                }
            } else {
                break;
            }
        }
        f2 = lastEven;
        return f2;
    }

    /**
     * @return double Percentage of odd Fibonacci numbers in interval
     * @see Fibonacci#percentageOddNumbers()
     */
    final double percentageOddNumbers() {
        int allNumbers = 0;
        double result = 0;
        for (int i = 0; i < fibonacci.length; i++) {
            if (fibonacci[i] <= number.lastNumAccess()) {
                if (fibonacci[i] >= number.firstNumAccess()) {
                    if (fibonacci[i] % 2 != 0) {
                        result++;
                    }
                    allNumbers++;
                }
            } else {
                break;
            }
        }
        result = (result * PERCENT) / (double) allNumbers;
        return result;
    }

}
