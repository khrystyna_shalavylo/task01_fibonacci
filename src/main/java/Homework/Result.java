package homework;

import java.util.Scanner;

/**
 * Class for realization of the tasks.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public final class Result {
    /**
     * @param sum - Sum object
     */
    private static Sum sum = new Sum();
    /**
     * @param fibonacci - Fibonacci object
     */
    private static Fibonacci fibonacci = new Fibonacci();
    /**
     * @param interval - Interval object
     */
    private static Interval interval = new Interval();
    /**
     * @param odd - OddNumbers object
     */
    private static OddNumbers odd = new OddNumbers();
    /**
     * @param in - Scanner object
     */
    private static Scanner in = new Scanner(System.in);
    /**
     * @param PERCENT - 100%
     */
    private static final int PERCENT = 100;

    /**
     * @see Result#Result()
     */
    private Result() {
    }

    /**
     * @param args - Argument
     * @see Result#main(String[])
     */
    public static void main(final String[] args) {
        System.out.println("Enter your interval.");
        interval.firstNumber();
        interval.lastNumber();
        interval.printingInterval();
        System.out.print("If you want print odd");
        System.out.println(" numbers from start enter 1, else - 0");
        if (in.nextInt() == 1) {
            odd.printOddNumbersStart();
        }
        System.out.print("If you want print");
        System.out.println(" odd numbers from the end enter 1, else - 0");
        if (in.nextInt() == 1) {
            odd.printOddNumbersEnd();
        }
        System.out.print("If you want print sum of odd numbers");
        System.out.println(" and sum of even numbers enter 1, else - 0");
        if (in.nextInt() == 1) {
            System.out.print("Sum of odd numbers: ");
            System.out.println(sum.summingOddNumbers());
            System.out.print("Sum of even numbers: ");
            System.out.println(sum.summingEvenNumbers());
        }
        System.out.print("If you want print the biggest odd Fibonacci");
        System.out.print(" number, the biggest even Fibonacci number");
        System.out.print("and percentage of Fibonacci odd numbers and ");
        System.out.println("even numbers enter 1, else - 0");
        if (in.nextInt() == 1) {
            System.out.println("Please enter count of fibonacci numbers.");
            fibonacci.fibonacciBuilding();
            System.out.print("The biggest odd fibonacci number: ");
            System.out.println(fibonacci.findingTheBiggestOddFibonacci());
            System.out.print("The biggest even fibonacci number: ");
            System.out.println(fibonacci.findingTheBiggestEvenFibonacci());
            if (fibonacci.accessF2() != -1 && fibonacci.accessF1() == -1) {
                System.out.println("Percentage of Fibonacci odd numbers: 0.0%");
                System.out.print("Percentage of Fibonacci even numbers:");
                System.out.println(" 100.0%");
            }
            if (fibonacci.accessF2() != -1 && fibonacci.accessF1() != -1) {
                System.out.print("Percentage of Fibonacci odd numbers: ");
                System.out.println(fibonacci.percentageOddNumbers() + "%");
                System.out.print("Percentage of Fibonacci even numbers: ");
                System.out.print(PERCENT - fibonacci.percentageOddNumbers());
                System.out.println("%");
            }
            if (fibonacci.accessF2() == -1 && fibonacci.accessF1() == -1) {
                System.out.println("Percentage of Fibonacci odd numbers: 0.0%");
                System.out.print("Percentage of Fibonacci even numbers:");
                System.out.println(" 0.0%");
            }
            if (fibonacci.accessF2() == -1 && fibonacci.accessF1() != -1) {
                System.out.print("Percentage of Fibonacci even numbers:");
                System.out.println(" 0.0%");
                System.out.print("Percentage of Fibonacci odd numbers:");
                System.out.println(" 100.0%");
            }
        }
    }
}
