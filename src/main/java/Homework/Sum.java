package homework;


/**
 * Class for summing numbers in interval.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public class Sum {
    /**
     * @param number - Interval object
     */
    private static Interval number = new Interval();

    /**
     * @return int Sum of odd numbers in interval
     * @see Sum#summingOddNumbers()
     */
    final int summingOddNumbers() {
        int start;
        int result = 0;
        if (number.firstNumAccess() % 2 == 0) {
            start = number.firstNumAccess() + 1;
        } else {
            start = number.firstNumAccess();
        }
        for (int i = start; i <= number.lastNumAccess(); i += 2) {
            result += i;
        }
        return result;
    }

    /**
     * @return int Sum of even numbers in interval
     * @see Sum#summingEvenNumbers()
     */
    final int summingEvenNumbers() {
        int start;
        int result = 0;
        if (number.firstNumAccess() % 2 == 0) {
            start = number.firstNumAccess();
        } else {
            start = number.firstNumAccess() + 1;
        }
        for (int i = start; i <= number.lastNumAccess(); i += 2) {
            result += i;
        }
        return result;
    }
}
