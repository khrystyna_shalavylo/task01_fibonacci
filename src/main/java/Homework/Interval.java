package homework;

import java.util.Scanner;

/**
 * Class for creating interval.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public class Interval {
    /**
     * @param firstNum - start of interval
     */
    private static int firstNum;
    /**
     * @param lastNum - end of interval
     */
    private static int lastNum;

    /**
     * @return int firstNum
     * @see Interval#firstNumAccess()
     */
    final int firstNumAccess() {
        return firstNum;
    }

    /**
     * @return int lastNum
     * @see Interval#lastNumAccess()
     */
    final int lastNumAccess() {
        return lastNum;
    }

    /**
     * @return int Entered number
     * @see Interval#entering()
     */
    static int entering() {
        String number;
        int intNumber;
        Scanner in = new Scanner(System.in);
        System.out.println("Your entering: ");
        number = in.nextLine();
        if (number.contains("^[a-zA-Z]+")) {
            System.out.println("Try again.");
            intNumber = -1;
        } else {
            intNumber = Integer.parseInt(number);
        }
        return intNumber;
    }

    /**
     * @return int Entered start of interval
     * @see Interval#firstNumber()
     */
    final int firstNumber() {
        int first;
        do {
            first = Interval.entering();
        } while (first < 0);
        firstNum = first;
        return firstNum;
    }

    /**
     * @return int Entered end of interval
     * @see Interval#lastNumber()
     */
    final int lastNumber() {
        int last;
        do {
            last = Interval.entering();
        } while (last < 0);
        lastNum = last;
        return lastNum;
    }

    /**
     * @see Interval#printingInterval()
     */
    final void printingInterval() {
        System.out.print("Your interval: [");
        System.out.println(firstNum + ";" + lastNum + "] ");
    }

}
