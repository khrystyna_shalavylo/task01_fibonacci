package homework;


/**
 * Class for doing operations under odd numbers.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public class OddNumbers {

    /**
     * @param number - Interval object
     */
    private static Interval number = new Interval();

    /**
     * @see OddNumbers#printOddNumbersStart()
     */
    final void printOddNumbersStart() {
        int start;
        if (number.firstNumAccess() % 2 == 0) {
            start = number.firstNumAccess() + 1;
        } else {
            start = number.firstNumAccess();
        }
        for (int i = start; i <= number.lastNumAccess(); i += 2) {
            System.out.println(i + " ");
        }
    }

    /**
     * @see OddNumbers#printOddNumbersEnd()
     */
    final void printOddNumbersEnd() {
        int end;
        if (number.lastNumAccess() % 2 == 0) {
            end = number.lastNumAccess() - 1;
        } else {
            end = number.lastNumAccess();
        }
        for (int i = end; i >= number.firstNumAccess(); i -= 2) {
            System.out.println(i + " ");
        }
    }
}
